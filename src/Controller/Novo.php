<?php

class Controller_Novo
{
    public function content($params)
    {

        $layout = new Prova_Layout(LAYOUT_PATH);

        $pdo = $params['pdo'];

        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {

            $sql = 'INSERT INTO tb_candidato_x_vaga
                VALUES (
                    NULL, :candidato, :vaga, :cadastro
                )';

            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(':candidato', $candidato);
            $stmt->bindParam(':vaga', $vaga);
            $stmt->bindParam(':cadastro', $cadastro);

            $candidato = filter_input(INPUT_POST, 'candidato', FILTER_SANITIZE_NUMBER_INT);
            $vaga = filter_input(INPUT_POST, 'vaga', FILTER_SANITIZE_NUMBER_INT);
            $cadastro = date('Y-m-d H:i:s');

            $stmt->execute();
            Prova_Utils::redirect();

        }

        $sql = 'SELECT
            candidatos.id_candidato AS id,
            candidatos.nome
                FROM tb_candidato AS candidatos';

        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $layout->candidatos = $stmt->fetchAll();

        $sql = 'SELECT
            vagas.id_vaga AS id,
            vagas.nome_vaga as nome
                FROM tb_vaga AS vagas';

        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $layout->vagas = $stmt->fetchAll();

        return $layout->showLayout($params['op']);
    }

}