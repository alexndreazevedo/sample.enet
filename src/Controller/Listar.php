<?php

class Controller_Listar
{
    public function content($params)
    {
        $layout = new Prova_Layout(LAYOUT_PATH);

        $pdo = $params['pdo'];

        $sql = 'SELECT
            relacao.id_candidato_vaga AS id,
            candidato.nome,
            vagas.nome_vaga AS vaga,
            relacao.data_cadastro AS cadastro
                FROM tb_candidato_x_vaga AS relacao
            INNER JOIN
                tb_candidato AS candidato
                    ON candidato.id_candidato = relacao.id_candidato
            INNER JOIN
                tb_vaga AS vagas
                    ON vagas.id_vaga = relacao.id_vaga
            LIMIT :pagina, 50';

        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':pagina', $pagina);
        $pagina = $params['pg'];

        $stmt->execute();
        $layout->total = $stmt->rowCount();
        $layout->result = $stmt->fetchAll();

        return $layout->showLayout($params['op']);
    }

}