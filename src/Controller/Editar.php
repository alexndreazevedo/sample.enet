<?php

class Controller_Editar
{
    public function content($params)
    {

        $layout = new Prova_Layout(LAYOUT_PATH);

        $pdo = $params['pdo'];

        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {

            $sql = 'UPDATE tb_candidato_x_vaga
                SET
                    id_candidato = :candidato, id_vaga = :vaga, data_cadastro = :cadastro
                
                WHERE
                    id_candidato_vaga = :id';

            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':candidato', $candidato);
            $stmt->bindParam(':vaga', $vaga);
            $stmt->bindParam(':cadastro', $cadastro);

			$id = $params['id'];
            $candidato = filter_input(INPUT_POST, 'candidato', FILTER_SANITIZE_NUMBER_INT);
            $vaga = filter_input(INPUT_POST, 'vaga', FILTER_SANITIZE_NUMBER_INT);
            $cadastro = date('Y-m-d H:i:s');
			
            $stmt->execute();
            Prova_Utils::redirect();

        }

        $sql = 'SELECT
            relacao.id_candidato_vaga AS id,
            relacao.id_candidato AS candidato,
            relacao.id_vaga AS vaga
                FROM tb_candidato_x_vaga AS relacao
            WHERE relacao.id_candidato_vaga = :id';

        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':id', $id);
        $id = $params['id'];

        $stmt->execute();
        $layout->candidatura = current($stmt->fetchAll());

        $sql = 'SELECT
            candidatos.id_candidato AS id,
            candidatos.nome
                FROM tb_candidato AS candidatos';

        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $layout->candidatos = $stmt->fetchAll();

        $sql = 'SELECT
            vagas.id_vaga AS id,
            vagas.nome_vaga as nome
                FROM tb_vaga AS vagas';

        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $layout->vagas = $stmt->fetchAll();

        return $layout->showLayout($params['op']);
    }

}