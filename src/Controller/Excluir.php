<?php

class Controller_Excluir
{
    public function content($params)
    {

        $layout = new Prova_Layout(LAYOUT_PATH);

        $pdo = $params['pdo'];

        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {

            $sql = 'DELETE FROM tb_candidato_x_vaga
                WHERE id_candidato_vaga = :id';

            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(':id', $id);
            $id = $params['id'];

            $stmt->execute();
            Prova_Utils::redirect();

        }

        $sql = 'SELECT
            relacao.id_candidato_vaga AS id,
            candidato.nome AS candidato,
            vagas.nome_vaga AS vaga,
            relacao.data_cadastro AS cadastro
                FROM tb_candidato_x_vaga AS relacao
            INNER JOIN
                tb_candidato AS candidato
                    ON candidato.id_candidato = relacao.id_candidato
            INNER JOIN
                tb_vaga AS vagas
                    ON vagas.id_vaga = relacao.id_vaga
            WHERE relacao.id_candidato_vaga = :id';

        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':id', $id);
        $id = $params['id'];

        $stmt->execute();
        $layout->candidatura = current($stmt->fetchAll());

        return $layout->showLayout($params['op']);
    }

}