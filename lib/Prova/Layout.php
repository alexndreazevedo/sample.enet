<?php

class Prova_Layout
{

    protected $_file;

    protected $_path;

    public $base;

    public $root;

    public function __construct($path = null, $base = null, $root = null) {

        $this->setLayout();
        $this->_path = $path;

        if($base == null){

            $this->base = 'http://' . SERVER . '/';

        }

        if($root == null){

            $this->root = 'http://' . SERVER . '/assets/';

        }


    }

    public function getLayout($params = null){

        if($params == null) {

            return $this->_file;

        } else {

            if(isset($this->_file[$params])){

                return $this->_path . DIRECTORY_SEPARATOR . $this->_file[$params];
            }

        }

        return false;

    }

    public function setLayout(){

        if($this->getLayout('html') == null){

            $this->_file = array(

                'default'   => 'layout/default.phtml',
                'listar'    => 'crud/select.phtml',
                'novo'      => 'crud/insert.phtml',
                'editar'    => 'crud/update.phtml',
                'excluir'   => 'crud/delete.phtml',

            );

        }

    }

    public function customLayout($params = array()) {

        array_push($this->_file, $params);

    }

    public function showLayout($layout = null, $options = null){

        if($options != null) {

            $this->customLayout($options);

        }

        $filename = $this->getLayout($layout);

        $file = Prova_File::getFile($filename);

        ob_start();

        include($filename);

        $return = ob_get_contents();

        ob_end_clean();

        return $return;

    }

}