<?php

class Prova_Connect
{

    protected $_pdo;

    protected $_server = 'localhost';

    protected $_dbname;

    protected $_user = null;

    protected $_pass = null;

    public function __construct()
    {

        $this->_server = 'localhost';
        $this->_dbname = 'prova_user';
        $this->_user = 'root';
        $this->_pass = '';

//		$this->_server = 'localhost';
//		$this->_dbname = 'marinho_prova';
//		$this->_user = 'marinho_prova';
//		$this->_pass = 'e.3OTd;SmmCl';

    }

    public function connect()
    {

        $this->_pdo = new PDO('mysql:host=' . $this->_server . ';dbname=' . $this->_dbname, $this->_user, $this->_pass);

        if($this->_pdo)
        {
            return $this->_pdo;
        }

        return false;

    }

}
