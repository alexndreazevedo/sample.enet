<?php

require_once 'Prova/Loader/Autoloader.php';

class Prova_Application
{

    protected $_autoloader;

    public $layout;

    const PROVA_SELECT = 'listar';

    const PROVA_INSERT = 'novo';

    const PROVA_UPDATE = 'editar';

    const PROVA_DELETE = 'excluir';

    public function __construct($environment = null, $options = null)
    {

        $this->_autoloader = Prova_Loader_Autoloader::getInstance();

        $this->load();

        $this->setConstruct($environment, $options);

    }

    public function load() {}

    public function run() {}

    public function setConstruct() {

        $this->layout = new Prova_Layout(LAYOUT_PATH);

    }

    public function setScreen($layout = 'default')
    {

        return $this->layout->showLayout($layout);

    }

    public function getRoute($pdo)
    {

        if(isset($_GET['route']))
        {
            $params = explode('/', $_GET['route']);
        }

        $route = array(
            'pdo' => $pdo,
            'op' => (isset($params[0]) && !empty($params[0]) ? $params[0] : false),
            'id' => (isset($params[1]) && !empty($params[1]) ? $params[1] : 0),
            'pg' => (isset($params[2]) && !empty($params[2]) ? $params[2] : 0),
        );

        return $route;
    }

    public function getContent($pdo)
    {

        $route = $this->getRoute($pdo);

        $crud = array(
            self::PROVA_SELECT,
            self::PROVA_INSERT,
            self::PROVA_UPDATE,
            self::PROVA_DELETE,
        );

        if(!$route['op'])
        {
            $route['op'] = self::PROVA_SELECT;
        }

        if(in_array($route['op'], $crud))
        {

            $this->layout->index = $route['op'];

            $class = 'Controller_' . ucfirst($route['op']);

            Prova_Loader::loadClass($class);

            $content = new $class;

            return $content->content($route);
        }

        return false;

    }

}
