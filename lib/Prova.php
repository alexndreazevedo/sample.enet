<?php

include_once 'Prova/Application.php';

class Prova extends Prova_Application
{

    public $pdo;

    public function load(){}  //@TODO

    public function run()
    {

        $connect = new Prova_Connect();
        $this->pdo = $connect->connect();

        Prova_Utils::boot();

        $this->layout->content = $this->getContent($this->pdo);
        $this->layout->title = 'Prova e-Net';

        if (!$this->layout->content)
        {
            header('HTTP/1.1 404 Not Found');
            die('404');
        }

        print $this->setScreen();

    }

}
