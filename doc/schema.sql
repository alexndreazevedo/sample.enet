-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 
-- Versão do Servidor: 5.5.24-log
-- Versão do PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `prova_user`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_candidato`
--

CREATE TABLE IF NOT EXISTS `tb_candidato` (
  `id_candidato` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  `data_nasc` datetime DEFAULT NULL,
  PRIMARY KEY (`id_candidato`),
  KEY `id_candidato` (`id_candidato`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `tb_candidato`
--

INSERT INTO `tb_candidato` (`id_candidato`, `nome`, `data_nasc`) VALUES
(2, 'Ana Pereira', '1985-08-05 00:00:00'),
(4, 'Eduardo', '1985-02-10 00:00:00'),
(5, 'João da Silva Matoso', '1979-03-17 00:00:00'),
(7, 'José da Silva', '1979-03-17 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_candidato_x_vaga`
--

CREATE TABLE IF NOT EXISTS `tb_candidato_x_vaga` (
  `id_candidato_vaga` int(11) NOT NULL AUTO_INCREMENT,
  `id_candidato` int(11) NOT NULL,
  `id_vaga` int(11) NOT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`id_candidato_vaga`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Extraindo dados da tabela `tb_candidato_x_vaga`
--

INSERT INTO `tb_candidato_x_vaga` (`id_candidato_vaga`, `id_candidato`, `id_vaga`, `data_cadastro`) VALUES
(35, 2, 1, '2012-10-08 00:00:00'),
(36, 4, 5, '2012-10-10 00:00:00'),
(37, 3, 0, '2012-10-08 00:00:00'),
(38, 2, 3, '0000-00-00 00:00:00'),
(39, 2, 2, '0000-00-00 00:00:00'),
(40, 2, 2, '0000-00-00 00:00:00'),
(41, 0, 0, '0000-00-00 00:00:00'),
(43, 4, 6, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_vaga`
--

CREATE TABLE IF NOT EXISTS `tb_vaga` (
  `id_vaga` int(11) NOT NULL AUTO_INCREMENT,
  `nome_vaga` varchar(25) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id_vaga`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `tb_vaga`
--

INSERT INTO `tb_vaga` (`id_vaga`, `nome_vaga`) VALUES
(1, 'Analista'),
(5, 'Gerente'),
(6, 'Web'),
(8, 'Webdeveloper');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
