<?php

header('Content-Type: text/html; charset=utf-8');

require_once 'Config.php';

set_include_path(implode(PATH_SEPARATOR, array(realpath(REAL_PATH . DIRECTORY_SEPARATOR . 'lib'), get_include_path())));
set_include_path(implode(PATH_SEPARATOR, array(realpath(REAL_PATH . DIRECTORY_SEPARATOR . 'src'), get_include_path())));

require_once 'Prova.php';

$prova = new Prova();

date_default_timezone_set('America/Fortaleza');

error_reporting(E_ALL|E_STRICT);