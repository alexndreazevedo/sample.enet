CKEDITOR.plugins.add( 'cdc',
{
    requires : ['iframedialog'],
    init : function( editor )
    {
        var pluginName = 'cdc';

        // Register the dialog.
        CKEDITOR.dialog.addIframe(pluginName, pluginName, FILE_SELECTOR, 960, 508, null, {buttons: [ CKEDITOR.dialog.cancelButton ]});


        // Register the command.
        var command = editor.addCommand(pluginName, {exec: function() {
            editor.openDialog(pluginName);
            CKEDITOR_HANDLERS.editor = editor;
        }});
        command.modes = {wysiwyg:1, source:0};
        command.canUndo = false;

        editor.ui.addButton('cdcInsertImage',
            {
                label: 'Inserir imagem',
                className: 'cke_button_image',
                command: pluginName
            });
    }
} );
