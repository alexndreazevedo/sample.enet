(function($){
    var methods = {
        init : function(options) {
            if (this.length)
            {
                var plugin = this;

                var settings = $.extend( {
                    'controls': 'carousel_controls',
                    'interval': 3000
                }, options);


                var carousel_controls = document.getElementById(settings.controls);

                plugin.carousel({
                    interval: settings.interval
                    });

                $('a', carousel_controls).click(function() {
                    plugin.carousel(parseInt($(this).text()) - 1);
                    return false;
                });

                plugin.bind('slide', function() {
                    $('li', carousel_controls).removeClass('active');
                });

                plugin.bind('slid', function() {
                    $('li:eq(' + $('.active', this).index() + ')', carousel_controls).addClass('active');
                });


            }
            return this;
        }
    };

    $.fn.numberedCarousel = function( method ) {

        // Method calling logic
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.numberedCarousel' );
        }

    };

})(jQuery);