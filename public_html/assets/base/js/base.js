$(function() {
    $('[rel=tooltip]').tooltip();

    $.fn.select2.defaults.placeholder = 'Selecione...';
    $.fn.select2.defaults.width = 'element';
    $.fn.select2.defaults.closeOnSelect = false;
    $.fn.select2.defaults.formatSelectionTooBig = function(limit) {
        return 'Máximo de ' + limit + ' opções.';
    }
    $.fn.select2.defaults.formatNoMatches = function () { return 'Nenhum item encontrado.'; }

    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

    $('.tel').mask('(99) 9999-9999');
    $('.cep').mask('99.999-999');
    $('.number').mask('9999999999');
    $('.cep').mask('99.999-999');
    $('.cpf').mask('999.999.999-99');

    setSelect('select');
    setDate('input.date');
    setDateTime('input.datetime');
    setInteger('input.integer');

    (function () {
        var config = {
            toolbar: 'Custom',
            scayt_sLang: 'pt_BR',
            toolbar_Custom: [
                { name: 'document', items : [ 'Source' /*,'-','Save','NewPage','DocProps','Preview','Print' */ ] },
                { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
                { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
                { name: 'links', items : [ 'Link','Unlink' ] },
                { name: 'insert', items : [ 'cdcInsertImage','Flash','Table','Smiley','SpecialChar','PageBreak' ] },
                { name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] },
                '/',
                { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
                { name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
                { name: 'colors', items : [ 'TextColor','BGColor' ] },
                { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
            ],
            removePlugins: 'font,forms',
            extraPlugins: 'cdc'
        };
        $('.rich').ckeditor(config);
    })();

    var renderFileList = function(el) {
        var val = $.parseJSON(decodeURIComponent($(el).val()));
        var list = $(el).siblings('.list');
        var prop;
        for (prop in val)
        {
            var newEl = $('<a class="colorbox" href="' + UPLOAD + val[prop].nome + '"><img src="' + UPLOAD + 'preview/' + val[prop].nome + '"></a>').colorbox({maxWidth: '500', maxHeight: '300'});
            var newElContainer = $('<div class="fileitem draggable">');
            newElContainer.append(newEl);
            newElContainer.append('<a href="#" class="removefile" data-fileid="' + prop + '"><i class="icon icon-trash"></i></a>');
            list.append(newElContainer);

        }

    }

    $('.file_area input').each(function(i, item) {
        renderFileList(item);
    });

    $('.sortable').sortable();

    //$('.droppable').droppable();

    $('.file_area button').click(function() {
        var filemanager = $(document.getElementById('filemanager_modal'));

        filemanager.find('iframe').attr('src', FILE_MANAGER.replace('__FILE_MANAGER__REGION__', $(this).prev().attr('name')));

        filemanager.dialog({
            width: '70%',
            height: 600,
            modal: true,
            buttons: [
                {
                    text: 'Fechar',
                    click: function() { $(this).dialog("close"); $('.sortable').sortable(); }
                }
            ]

        });

        return false;
    });

    $('.removefile').on('click', function() {
        var input = $(this).parents('.file_area').find('input');
        var val = $.parseJSON(decodeURIComponent(input.val()));
        var index = $(this).attr('data-fileid');
        delete val[index];
        input.val(encodeURIComponent(JSON.stringify(val)));
        $(this).parent().remove();
        return false;
    });

});

function setSelect(classe) {

    $(classe).each(function(i, item) {
        var diz = $(this);
        if (diz.hasClass('limited'))
        {
            $(item).select2({
                maximumSelectionSize: 3
            });
        }
        else if (diz.hasClass('limited10'))
        {
            $(item).select2({
                maximumSelectionSize: 10
            });
        }
        else
        {
            $(item).select2();
        }

    });

}

function setDateTime(classe) {
    if ($(classe).length)
    {
        $(classe).datetimepicker({
            duration: '',
            showTime: true,
            constrainInput: false,
            changeMonth: true,
            changeYear: true,
            minDate: -100

        });
    }
}

function setDate(classe) {
    if ($(classe).length)
    {
        $(classe).datepicker({
            duration: '',
            showTime: false,
            constrainInput: false,
            dateFormat: 'dd/mm/yy',
            yearRange: '-100Y:+0Y',
            changeMonth: true,
            changeYear: true
        }).mask('99/99/9999');
    }
}

function setInteger(classe) {

    $(classe).each(function () {

        min = $(this).attr('data-min');
        max = $(this).attr('data-max');
        prefix = $(this).attr('data-prefix');
        format = 'C';

        if(prefix){

            if(Globalize) Globalize.addCultureInfo("lang", { name: "lang", numberFormat: { ",": ".", ".": ",", percent: { pattern: ["-n %", "n %"], ",": ".", ".": ",", symbol: "%" }, currency: { pattern: ["$ -n", "$ n"], ",": ".", ".": ",", symbol: prefix } } });

            if(prefix == '%') format = 'P';

            $(this).numeric({
                format: format,
                culture: 'lang',
                min: min,
                max: max
            });

        } else {

            $(this).spinner({
                min: min,
                max: max
            });

        }
    });
}